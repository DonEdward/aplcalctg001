#include<iostream>
#include <math.h>

using namespace std;


class Cir
{
public:
	float xPos;
	float yPos;
	float radius;
	
	Cir(float p_x, float p_y, float p_radius);

	bool hDifferenceCir(Cir p_cirToCheck);
};

Cir::Cir(float p_x, float p_y, float p_radius)
{
	xPos = p_x;
	yPos = p_y;
	radius = p_radius;

}

bool Cir::hDifferenceCir(Cir p_cirToCheck)
{
	bool l_result = false;

	bool l_hDifference = false;

	float aLeg = xPos - p_cirToCheck.xPos;
	float bLeg = yPos - p_cirToCheck.yPos;
	
	float hTriangle = sqrt(pow(aLeg, 2) + pow(bLeg, 2));

	//check if hypotenuse is less than or equal to the sum of the radii of the circles

	if (hTriangle <= radius + p_cirToCheck.radius)
	{
		l_hDifference = true;
	}

	l_result = l_hDifference;
			return l_result;
}

int main()
{

	Cir cirA = Cir(10, 10, 5);
	Cir cirB = Cir(5, 5, 15);

	if (cirA.hDifferenceCir(cirB))
	{
		cout << "Circles A and B intersect" << endl;
	}
	else
	{
		cout << "Circles A and B did not intersect" << endl;
	}

	system("pause");
	return 0;
}
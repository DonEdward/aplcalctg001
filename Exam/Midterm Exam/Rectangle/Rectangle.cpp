#include<iostream>

using namespace std;

//Origin at lower-left
class Rect
{
public:
	int xPos;
	int yPos;
	int width;
	int height;
	int xAnchor;
	int yAnchor;

	Rect(int p_x, int p_y, int p_width, int p_height, int p_xA, int p_yA);

	bool didIntersectRect(Rect p_rectToCheck);
};

Rect::Rect(int p_x, int p_y, int p_width, int p_height,int p_xA,int p_yA)
{
	xPos = p_x;
	yPos = p_y;
	width = p_width;
	height = p_height;
	xAnchor = p_xA;
	yAnchor = p_yA;
}

bool Rect::didIntersectRect(Rect p_rectToCheck)
{
	bool l_result = false;

	bool l_xIntersect = false;
	bool l_yIntersect = false;

	if (xAnchor == 0 && yAnchor == 0)
	{
		//check if intersects x-axis
		if (xPos >= p_rectToCheck.xPos && xPos <= p_rectToCheck.xPos + p_rectToCheck.width)
		{
			l_xIntersect = true;
		}
		else if (xPos + width >= p_rectToCheck.xPos
			&& xPos + width <= p_rectToCheck.xPos + p_rectToCheck.width )
		{
			l_xIntersect = true;
		}

		//check if intersects y-axis
		if (yPos >= p_rectToCheck.yPos && yPos <= p_rectToCheck.yPos + p_rectToCheck.height)
		{
			l_yIntersect = true;
		}
		else if (yPos + height >= p_rectToCheck.yPos
			&& yPos + height <= p_rectToCheck.yPos + p_rectToCheck.height)
		{
			l_yIntersect = true;
		}
	}

	else if (xAnchor == 1 && yAnchor == 1)
	{
		//check if intersects x-axis
		if (xPos >= p_rectToCheck.xPos && xPos <= p_rectToCheck.xPos + p_rectToCheck.width/2)
		{
			l_xIntersect = true;
		}
		else if (xPos + width/2 >= p_rectToCheck.xPos
			&& xPos + width/2 <= p_rectToCheck.xPos + p_rectToCheck.width/2)
		{
			l_xIntersect = true;
		}

		//check if intersects y-axis
		if (yPos >= p_rectToCheck.yPos && yPos <= p_rectToCheck.yPos + p_rectToCheck.height/2)
		{
			l_yIntersect = true;
		}
		else if (yPos + height/2 >= p_rectToCheck.yPos
			&& yPos + height/2 <= p_rectToCheck.yPos + p_rectToCheck.height/2)
		{
			l_yIntersect = true;
		}
	}

	l_result = l_xIntersect && l_yIntersect;

	return l_result;
}

int main()
{
	Rect rectA = Rect(10, 10, 15, 15, 0, 0);
	Rect rectB = Rect(10, 5, 15, 15, 0, 0);

	if (rectA.didIntersectRect(rectB))
	{
		cout << "Rectangles A and B intersect";
	}
	else
	{
		cout << "Rectangles A and B did not intersect";
	}



	system("pause");
	return 0;
}